# AssetNavigator

![](screenshot.png)

The Jetbrains Plugin for navigating and developing Image and Video Operations the easy way

[TOC]

## Installation

 - Open your Jetbrains IDE _(tested on IntelliJ and CLion, but should also work in Android Studio, PyCharm, WebStorm, ...)_
 - Go to `Settings` > `Plugins`
 - Click on the gear icon at the top
 - Click "`Manage Plugin Repositories...`"
 - Click "`+`" to add a new repository
 - use this URL: [https://bitbucket.org/ErikBrendel/assetnavigator/raw/master/updatePlugins.xml](https://bitbucket.org/ErikBrendel/assetnavigator/raw/master/updatePlugins.xml) (right click -> `Copy Link Location`)
 - search for the "AssetNavigator" plugin in the list of available plugins
 - install it!
 - easily keep up to date via `Help` > `Check for Updates...`

## When first using the plugin...

...it might take several minutes to index your whole asset tree. After that, each re-start of your IDE (and especially each file modifiction durign runtime) should be significantly faster.
 
## Usage

Open up any asset xml file and click on the **icons on the side** to navigate through the file structure.

## Contributing

If you have any bug reports / feature requests, feel free to leave an issue here or message me directly.

Refer to [this guide](http://www.jetbrains.org/intellij/sdk/docs/welcome.html) for documentation on the Jetbrains Plugin API

## Shipping Updates

Several steps are required to successfully ship an update via the enterprise plugin repository:

 - Think of a new and higher version number (if the IDE detects that the version numbers are the same, it does not download and install the new plugin version)
 - Adjust the version number in `resuorces/META-INF/plugin.xml`.
 - In your IDE, click `Build` > `Prepare Plugin Module "AssetNavigator" for Deployment`.
 - Adjust the version number in `updatePlugins.xml` (this always has to be the same as in `plugin.xml` in the compiled jar)
 - commit updated jar and `updatePlugins.xml` to the master of this repository.