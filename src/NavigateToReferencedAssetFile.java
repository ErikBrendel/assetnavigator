import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiPlainText;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlText;
import org.jetbrains.annotations.NotNull;
import com.intellij.openapi.util.IconLoader;
import util.AssetFileNavigator;

import javax.swing.*;
import java.util.*;

/**
 * This class encapsulates the functionality of following direct asset file references:
 * Each xml attribute and text is checked if it is an asset reference (starting with "default/").
 * If it is, "navigation gutter icons" are added to follow that reference
 *
 * @see <a href="http://www.jetbrains.org/intellij/sdk/docs/tutorials/custom_language_support/line_marker_provider.html">custom language &gt; line marker provider</a>
 * @see <a href="https://github.com/JetBrains/intellij-sdk-docs/tree/master/code_samples/simple_language_plugin/src/com/simpleplugin">simple language plugin</a>
 */
public class NavigateToReferencedAssetFile {

    public static final Icon NAVIGATE_ICON = IconLoader.getIcon("/icons/asset_navigation.png");
    public static final Icon NAVIGATE_LOCALE_ICON = IconLoader.getIcon("/icons/asset_navigation_locale.png");
    public static final Icon NAVIGATE_ICON_MULTIPLE = IconLoader.getIcon("/icons/asset_navigation_multiple.png");
    public static final Icon NAVIGATE_ICON_BACK = IconLoader.getIcon("/icons/asset_navigation_back.png");
    public static final Icon NAVIGATE_ICON_MULTIPLE_BACK = IconLoader.getIcon("/icons/asset_navigation_multiple_back.png");
    public static final Icon NAVIGATE_ICON_ERROR = IconLoader.getIcon("/icons/asset_navigation_error.png");

    /**
     * entry point for the IDE for XML files
     */
    public static class Xml extends RelatedItemLineMarkerProvider {
        @Override
        protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo> result) {
            if (element instanceof XmlAttribute) {
                XmlAttribute xmlToken = (XmlAttribute) element;
                addNavigationGutterIconMaybe(element, result, xmlToken.getValue());
            } else if (element instanceof XmlText) {
                XmlText xmlText = (XmlText) element;
                addNavigationGutterIconMaybe(element, result, xmlText.getText());
            } else if (element instanceof XmlTag) {
                XmlTag xmlTag = (XmlTag) element;
                XmlFile containingFile = (XmlFile) xmlTag.getContainingFile();
                if (xmlTag == containingFile.getRootTag()) {
                    navigateToReferencingFiles(element, containingFile, result);
                }
            }
        }
    }

    /**
     * entry point for the IDE for plain text files (used for the entrypoint file)
     */
    public static class Text extends RelatedItemLineMarkerProvider {
        @Override
        protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo> result) {
            if (element instanceof PsiPlainText) {
                //might be the entrypoint file
                if (element.getContainingFile().getName().equals("entrypoint")) {
                    addNavigationGutterIconMaybe(element, result, AssetFileNavigator.fileContent(element.getContainingFile()));
                }
            }
        }
    }

    private static String parseToUri(String fullPath) {
        String[] defaultSplit = fullPath.split("/default/", 2);
        if (defaultSplit.length == 1) {
            return null;
        }
        String last = defaultSplit[defaultSplit.length - 1];
        String[] lastPathSplit = last.split("/");
        if (lastPathSplit.length < 2) {
            return null;
        }
        return "default/" + last;
    }

    /**
     * add a navigation gutter icon to the targetElement for navigating to the usages of targetFile
     */
    private static void navigateToReferencingFiles(@NotNull PsiElement targetElement, XmlFile targetFile, @NotNull Collection<? super RelatedItemLineMarkerInfo> result) {
        String myUri = parseToUri(targetFile.getVirtualFile().getPath());
        if (myUri == null) {
            return;
        }
        Collection<PsiFile> referencingAssetFiles = AssetFileNavigator.findReferencingAssetFiles(myUri);
        NavigationGutterIconBuilder<PsiElement> builder;
        if (referencingAssetFiles.size() == 0) {
            return;
        } else if (referencingAssetFiles.size() == 1) {
            builder = NavigationGutterIconBuilder.create(NAVIGATE_ICON_BACK);
        } else {
            builder = NavigationGutterIconBuilder.create(NAVIGATE_ICON_MULTIPLE_BACK);
        }
        builder.setTooltipText("Usages of " + myUri);
        builder.setTargets(referencingAssetFiles);
        result.add(builder.createLineMarkerInfo(targetElement));
    }

    /**
     * if possibleUri is actually a valid one, add the navigation to this referenced file(s) to the targetElement
     * @param targetElement
     * @param result
     * @param possibleUri
     */
    private static void addNavigationGutterIconMaybe(@NotNull PsiElement targetElement, @NotNull Collection<? super RelatedItemLineMarkerInfo> result, String possibleUri) {
        if (possibleUri != null && possibleUri.startsWith("default/")) {
            Collection<PsiElement> assetDefinitions = AssetFileNavigator.findReferencedAssetFiles(possibleUri);
            NavigationGutterIconBuilder<PsiElement> builder;

            if (AssetFileNavigator.isLocaleUrl(possibleUri)) {
                builder = NavigationGutterIconBuilder.create(NAVIGATE_LOCALE_ICON);
                builder.setTooltipText("Navigate to localized file");
            } else {
                if (assetDefinitions.size() == 0) {
                    builder = NavigationGutterIconBuilder.create(NAVIGATE_ICON_ERROR);
                    builder.setTooltipText("No such asset file found");
                } else if (assetDefinitions.size() == 1) {
                    builder = NavigationGutterIconBuilder.create(NAVIGATE_ICON);
                    String[] slashSplit = possibleUri.split("/");
                    String localFileName = slashSplit[slashSplit.length - 1];
                    builder.setTooltipText("Navigate to " + localFileName);
                } else {
                    builder = NavigationGutterIconBuilder.create(NAVIGATE_ICON_MULTIPLE);
                    builder.setTooltipText("Navigate to " + possibleUri.substring("default/".length()) + "...");
                }

            }
            builder.setTargets(assetDefinitions);
            result.add(builder.createLineMarkerInfo(targetElement));
        }
    }
}
