import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import util.graph.ReferenceCache;

/**
 * IDE entry point for when the user wants to clean the cache.
 * It deletes the file graph, so that it has to get re-created.
 */
public class InvalidateCacheAction extends AnAction {

    public InvalidateCacheAction() {
        this("InvalidateCacheAction");
    }

    public InvalidateCacheAction(String name) {
        super(name);
    }

    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        ReferenceCache.getInstance().invalidateCache();
    }
}