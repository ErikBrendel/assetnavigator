package syntax;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.openapi.util.IconLoader;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlTag;
import org.jetbrains.annotations.NotNull;
import util.NameExtendedPsiElement;
import util.PsiElementProxy;
import util.RenamedPsiElement;

import javax.swing.*;
import java.util.*;
import java.util.function.Function;

/**
 * These are the "navigation objects" that this codebase sometimes talks about.
 * A navigation is a series of steps that navigates the user automatically from one thing to another.
 * These things might be XML tags or attributes, files or even PsiElements of other languages.
 */
public class SyntaxBasedAssetNavigation {

    public static final Icon NAVIGATE_ICON_FATAL_ERROR = IconLoader.getIcon("/icons/internal_error.png");
    public static final Icon NAVIGATE_ICON_ERROR = IconLoader.getIcon("/icons/error.png");
    public static final Icon NAVIGATE_ICON_WARNING = IconLoader.getIcon("/icons/warning.png");
    public static final Icon NAVIGATE_ICON_INFO = IconLoader.getIcon("/icons/info.png");

    /** the steps to execute for this navigation */
    private final List<SearchStep> steps;
    /**
     * what that thing is called that the user wants to navigate to
     * (he will see "go to <displayName>" when hovering the navigation gutter icon)
     */
    private String displayName;
    /**
     * A pattern for renaming the found result nodes.
     * "pattern" means, that it is a string which can
     * make use of variables collected in the navigation.
     * Leave it at null to not rename the results
     */
    private String targetNamePattern;
    /** the icon to use for this navigation when everything works as expected */
    private Icon defaultIcon;
    private Map<String, Function<String, String>> functions = new HashMap<>();

    /**
     * @param searchSteps the source code of this navigation
     * @param referencedObjectName what the target is called, will be used like "cannot find <name>: <Error>", "go to <name>"
     * @param defaultIcon gets displayed when everything worked
     * @param targetNamePattern the pattern for the display names of found nodes. Leave null if no renaming is wanted.
     */
    public static SyntaxBasedAssetNavigation fromString(String searchSteps, String referencedObjectName, Icon defaultIcon, String targetNamePattern) {
        SyntaxBasedAssetNavigation navigation = SyntaxParser.parse(searchSteps);
        navigation.displayName = referencedObjectName;
        navigation.defaultIcon = defaultIcon;
        navigation.targetNamePattern = targetNamePattern;
        return navigation;
    }

    /**
     * Overloaded method without renaming the found nodes
     * @see #fromString(String, String, Icon, String)
     */
    public static SyntaxBasedAssetNavigation fromString(String searchSteps, String referencedObjectName, Icon defaultIcon) {
        return fromString(searchSteps, referencedObjectName, defaultIcon, null);
    }

    SyntaxBasedAssetNavigation(List<SearchStep> steps) {
        this.steps = steps;
    }

    /**
     * execute this navigation and display the resulting navigation gutter icon
     * @param elementToTest the start element to begin navigating from
     * @param result the collection to add the resulting icons to
     */
    public void addNavigationGutterIcon(XmlTag elementToTest, @NotNull Collection<? super RelatedItemLineMarkerInfo> result) {

        Set<SearchState> currentStates = new HashSet<>();
        currentStates.add(SearchState.fromInitialNode(new SearchNode.XmlTagNode(elementToTest)));

        // executing the steps one by one on the set of current states,
        // aborting once this set becomes empty
        for (SearchStep step : steps) {
            Set<SearchState> futureStates = new HashSet<>();
            for (SearchState oldState : currentStates) {
                futureStates.addAll(step.apply(oldState, this));
            }

            if (futureStates.isEmpty()) {
                // create a nice debug view on the old states
                Set<String> oldVariableStates = new HashSet<>();
                for (SearchState state : currentStates) {
                    oldVariableStates.add(state.dumpVariables());
                }
                StringBuilder variablesDump = new StringBuilder("{\n");
                for (String vars : oldVariableStates) {
                    variablesDump.append(vars);
                }
                // display the failure of the navigation to the user
                handleFailure(result, step, elementToTest, variablesDump.append("}").toString());
                return;
            }

            currentStates = futureStates;
        }

        // after execution of all the steps, extract the result nodes from the remaining SearchStates
        List<PsiElement> rawResultNodes = new ArrayList<>(currentStates.size());
        for (SearchState resultState : currentStates) {
            PsiElement psi = resultState.getCurrentNode().asPsi();
            if (targetNamePattern != null) {
                psi = new RenamedPsiElement(psi, getResultNameFor(resultState));
            } else {
                psi = new PsiElementProxy(psi); // to prevent the file path from being displayed
            }
            rawResultNodes.add(psi);
        }

        // make result nodes further distinctable by adding path hints to their display names if needed
        List<PsiElement> resultNodes;
        if (areInDifferentFiles(rawResultNodes)) {
            resultNodes = new ArrayList<>(rawResultNodes.size());
            for (PsiElement element : rawResultNodes) {
                resultNodes.add(addPathHint(element, rawResultNodes));
            }
        } else {
            resultNodes = rawResultNodes;
        }

        // finally add the navigation gutter icon to the result collection
        NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(defaultIcon);
        builder.setTooltipText("Go to " + displayName);
        builder.setTargets(resultNodes);
        result.add(builder.createLineMarkerInfo(elementToTest));
    }

    /**
     * take care of the case that a SearchStep produced no next SearchStates
     * @param failingStep the step that caused the navigation to die
     * @param lineMarkerPosition where this navigation started from
     * @param variablesDump the debug text of the last non-empty SearchState-Set
     */
    private void handleFailure(Collection<? super RelatedItemLineMarkerInfo> result, SearchStep failingStep, PsiElement lineMarkerPosition, String variablesDump) {
        int failingIndex = steps.indexOf(failingStep);
        assert(failingIndex >= 0);

        // find out the error level that this navigation stopped at
        SearchStep.ErrorLevel lastErrorLevel = SearchStep.ErrorLevel.NONE;
        for (int i = failingIndex; i >= 0; i--) {
            SearchStep.ErrorLevel level = steps.get(i).getErrorLevel();
            if (level != SearchStep.ErrorLevel.NONE) {
                lastErrorLevel = level;
                break;
            }
        }
        if (lastErrorLevel == SearchStep.ErrorLevel.NONE) {
            return; // it seems like this failure is according to plan, so silently do nothing
        }

        // find the appropriate decoration for the message
        String errorPrefix = "Error";
        Icon icon = NAVIGATE_ICON_ERROR;
        if (lastErrorLevel == SearchStep.ErrorLevel.WARN) {
            errorPrefix = "Warning";
            icon = NAVIGATE_ICON_WARNING;
        }
        if (lastErrorLevel == SearchStep.ErrorLevel.INFO) {
            errorPrefix = "Info";
            icon = NAVIGATE_ICON_INFO;
        }
        if (lastErrorLevel == SearchStep.ErrorLevel.FATAL) {
            errorPrefix = "Internal Error";
            icon = NAVIGATE_ICON_FATAL_ERROR;
        }

        // create the failure icon with
        NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(icon);
        builder.setTargets(); //no click effect
        builder.setTooltipText(errorPrefix + ": Cannot find " + displayName + ". " + failingStep.errorString() + "  \nVariables: " + variablesDump);
        result.add(builder.createLineMarkerInfo(lineMarkerPosition));
    }

    /**
     * if there is a targetNamePattern to alter the presentation of results,
     * use this method to generate their display name
     */
    private String getResultNameFor(SearchState resultState) {
        String replacedString = targetNamePattern;
        for (String variableName : resultState.allVariableNames()) {
            String replacement = resultState.getVariable(variableName).content();
            replacedString = replacedString.replace("$" + variableName, replacement);
        }
        return replacedString;
    }

    /**
     * @return whether this navigation contains any backwards searching steps
     *         and thus needs the file graph in order to get executed
     */
    public boolean needsFileGraph() {
        for (SearchStep step : steps) {
            if (step.needsFileGraph()) {
                return true;
            }
        }
        return false;
    }

    private static final List<Character> PATH_SEPARATORS = Arrays.asList('/', '\\', '.');

    /**
     * Crazy algorithm that finds the unique core of a path amongst a list of reference paths,
     *       cutting away common prefixes and suffixes, replacing them with "..."
     * This method adds " in <shortPathInfo>" to the name of the given target PsiElement
     * @param target the PsiElement to add additional path info to
     * @param elements the other elements whose paths get compared against the target elements path
     * @return a NameExtendedPsiElement around the target element
     * @throws Exception badly if you give it a list where all files have the same path
     */
    @NotNull
    private static PsiElement addPathHint(PsiElement target, List<PsiElement> elements) {
        List<String> paths = new ArrayList<>(elements.size()); // these strings cannot be all equal
        String myPath = target.getContainingFile().getVirtualFile().getPath();
        int shortestLength = myPath.length();
        for (PsiElement e : elements) {
            String path = e.getContainingFile().getVirtualFile().getPath();
            paths.add(path);
            shortestLength = Math.min(shortestLength, path.length());
        }

        // find the interesting core of the paths, cut off identical and postfixes
        int prefixLength = -1;
        int testPrefixLength = -1;
        //find prefix length
        while (testPrefixLength < shortestLength - 1) {
            //try to increase the prefix
            testPrefixLength++;
            boolean isSamePrefix = true;
            for (int p = 0; p < paths.size() - 1 && isSamePrefix; p++) {
                if (paths.get(p).charAt(testPrefixLength) != paths.get(p + 1).charAt(testPrefixLength)) {
                    isSamePrefix = false;
                }
            }
            if (!isSamePrefix) break;
            if (PATH_SEPARATORS.contains(myPath.charAt(testPrefixLength))) {
                prefixLength = testPrefixLength;
            }
        }

        int postfixLength = -1;
        int testPostfixLength = -1;
        while (testPostfixLength < shortestLength - 1) {
            testPostfixLength++;
            boolean isSamePostfix = true;
            for (int p = 0; p < paths.size() - 1 && isSamePostfix; p++) {
                String path1 = paths.get(p);
                String path2 = paths.get(p + 1);
                if (path1.charAt(path1.length() - testPostfixLength - 1) != path2.charAt(path2.length() - testPostfixLength - 1)) {
                    isSamePostfix = false;
                }
            }
            if (!isSamePostfix) break;
            if (PATH_SEPARATORS.contains(myPath.charAt(myPath.length() - testPostfixLength - 1))) {
                postfixLength = testPostfixLength;
            }
        }

        String result = myPath;
        if (prefixLength > 3) {
            result = "..." + result.substring(prefixLength);
        }
        if (postfixLength > 3) {
            result = result.substring(0, result.length() - postfixLength) + "...";
        }

        return new NameExtendedPsiElement(target, " in " + result);
    }

    /**
     * @return whether these PsiElements scattered across at least two different files
     */
    private static boolean areInDifferentFiles(List<PsiElement> elements) {
        Set<String> paths = new HashSet<>(2);
        for (PsiElement e : elements) {
            paths.add(e.getContainingFile().getVirtualFile().getPath());
            if (paths.size() > 1) {
                return true;
            }
        }
        return false;
    }

    public SyntaxBasedAssetNavigation registerFunction(String name, Function<String, String> f) {
        this.functions.put(name, f);
        return this;
    }

    public Function<String, String> functionNamed(String name) {
        return this.functions.getOrDefault(name, null);
    }
}
