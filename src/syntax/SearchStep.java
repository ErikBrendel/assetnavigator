package syntax;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import util.AssetFileNavigator;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A single step of a navigation.
 * It gets applied onto a SearchState and produces zero, one or more resulting SearchSteps.
 *
 * Asserts are searchSteps that do not move the current head of the navigation:
 * They either do nothing (if their assertion is fulfilled), or kill the navigation (if not)
 */
public abstract class SearchStep {

    /** which error to display when this step kills the navigation */
    public String errorString() {
        return "Not found.";
    }

    /** different severities of killing a navigation */
    public enum ErrorLevel {
        NONE, INFO, WARN, ERROR, FATAL
    }

    /** the main method that applies this step onto a SearchState */
    @NotNull
    public abstract Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context);

    /** the level of error that this SearchStep implies */
    public ErrorLevel getErrorLevel() {
        return ErrorLevel.NONE;
    }

    /** whether this SearchStep needs to perform expensive backwards searching via the file graph */
    public boolean needsFileGraph() {
        return false;
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    /** just passing through the state, doing nothing */
    public static class Void extends SearchStep {
        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            return Collections.singleton(currentState);
        }
    }

    /** always kills the navigation, with a designated error message */
    public static class ErrorMessage extends SearchStep {
        private final String errorMessage;

        public ErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            return Collections.emptySet();
        }

        @Override
        public String errorString() {
            return errorMessage;
        }

        @Override
        public ErrorLevel getErrorLevel() {
            return ErrorLevel.FATAL;
        }
    }

    /** finds all child nodes with a given name */
    public static class FindChild extends SearchStep {
        private final String childName;

        public FindChild(String childName) {
            this.childName = childName;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            Collection<SearchState> result = new HashSet<>();
            for (SearchNode childNode : currentState.getCurrentNode().getChildrenWithName(childName)) {
                result.add(currentState.withAdjustedCurrentNode(childNode));
            }
            return result;
        }

        @Override
        public String errorString() {
            return "No child named " + childName;
        }
    }

    /** finds all children, without any name check */
    public static class FindAllChildren extends SearchStep {

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            Collection<SearchState> result = new HashSet<>();
            for (SearchNode childNode : currentState.getCurrentNode().getAllChildren()) {
                result.add(currentState.withAdjustedCurrentNode(childNode));
            }
            return result;
        }

        @Override
        public String errorString() {
            return "Node has no children";
        }
    }

    /** navigates to the parent node */
    public static class GoToParent extends SearchStep {

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            SearchNode parent = currentState.getCurrentNode().parent();
            if (parent == null) {
                return Collections.emptySet();
            }
            return Collections.singleton(currentState.withAdjustedCurrentNode(parent));
        }

        @Override
        public String errorString() {
            return "No parent to go to";
        }
    }

    /** navigates to the parent node if its name equals the given one */
    public static class GoToNamedParent extends SearchStep {

        private final String parentName;

        public GoToNamedParent(String parentName) {
            this.parentName = parentName;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            SearchNode parent = currentState.getCurrentNode().parent();
            if (parent == null || !parent.name().equals(parentName)) {
                return Collections.emptySet();
            }
            return Collections.singleton(currentState.withAdjustedCurrentNode(parent));
        }

        @Override
        public String errorString() {
            return "No parent of type " + parentName;
        }
    }

    /** navigates to the attribute with the given name */
    public static class GoToAttribute extends SearchStep {

        private final String attributeName;

        public GoToAttribute(String attributeName) {
            this.attributeName = attributeName;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            SearchNode attribute = currentState.getCurrentNode().getAttribute(attributeName);
            if (attribute == null) {
                return Collections.emptySet();
            }
            return Collections.singleton(currentState.withAdjustedCurrentNode(attribute));
        }

        @Override
        public String errorString() {
            return "No attribute named " + attributeName;
        }
    }

    /** adds the current node to the list of variables under the given name, not altering the current node */
    public static class SaveCurrentNode extends SearchStep {

        private final String variableName;

        public SaveCurrentNode(String variableName) {
            this.variableName = variableName;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            return Collections.singleton(currentState.withCurrentNodeAsVariable(variableName));
        }
    }

    /** jumps back to a node that was previously added to the list of variables */
    public static class GoToSaveMark extends SearchStep {

        private final String variableName;

        public GoToSaveMark(String variableName) {
            this.variableName = variableName;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            SearchNode savedNode = currentState.getVariable(variableName);
            if (savedNode == null) {
                return Collections.emptySet();
            }
            return Collections.singleton(currentState.withAdjustedCurrentNode(savedNode));
        }

        @Override
        public String errorString() {
            return "Syntax error: No saved SearchNode with name " + variableName;
        }
    }

    /** assertion that compares the content of the current node with the one of a previously saved variable */
    public static class AssertContentEqualsOther extends SearchStep {

        private final String variableName;

        public AssertContentEqualsOther(String variableName) {
            this.variableName = variableName;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            SearchNode savedNode = currentState.getVariable(variableName);
            if (savedNode == null) {
                return Collections.emptySet();
            }
            String compareValue = savedNode.content();
            String myValue = currentState.getCurrentNode().content();
            if (!compareValue.equals(myValue)) {
                return Collections.emptySet();
            }
            return Collections.singleton(currentState);
        }

        @Override
        public String errorString() {
            return "No Node with same content as " + variableName;
        }
    }

    /** assertion that compares the name of the current node with a given string literal */
    public static class AssertNameEqualsLiteral extends SearchStep {

        private final String compareString;

        public AssertNameEqualsLiteral(String compareString) {
            this.compareString = compareString;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            String myName = currentState.getCurrentNode().name();
            if (!myName.equals(compareString)) {
                return Collections.emptySet();
            }
            return Collections.singleton(currentState);
        }

        @Override
        public String errorString() {
            return "No Node with name \"" + compareString + "\"";
        }
    }

    /** assertion that compares the content of the current node with a given string literal */
    public static class AssertContentEqualsLiteral extends SearchStep {

        private final String compareString;

        public AssertContentEqualsLiteral(String compareString) {
            this.compareString = compareString;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            String myValue = currentState.getCurrentNode().content();
            if (!myValue.equals(compareString)) {
                return Collections.emptySet();
            }
            return Collections.singleton(currentState);
        }

        @Override
        public String errorString() {
            return "No Node with value \"" + compareString + "\"";
        }
    }

    /** changing the error level to INFO */
    public static class ErrorLevelInfo extends SearchStep {
        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            return Collections.singleton(currentState);
        }

        @Override
        public ErrorLevel getErrorLevel() {
            return ErrorLevel.INFO;
        }
    }

    /** changing the error level to WARN */
    public static class ErrorLevelWarn extends SearchStep {
        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            return Collections.singleton(currentState);
        }

        @Override
        public ErrorLevel getErrorLevel() {
            return ErrorLevel.WARN;
        }
    }

    /** changing the error level to ERROR */
    public static class ErrorLevelError extends SearchStep {
        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            return Collections.singleton(currentState);
        }

        @Override
        public ErrorLevel getErrorLevel() {
            return ErrorLevel.ERROR;
        }
    }

    /** navigate to the files that are referenced by the current node */
    public static class GoToReferencedFile extends SearchStep {
        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            String uri = currentState.getCurrentNode().content();
            Collection<PsiElement> files = AssetFileNavigator.findReferencedAssetFiles(uri);
            if (files.isEmpty()) {
                return Collections.emptySet();
            }
            Set<SearchState> nextStates = new HashSet<>();
            for (PsiElement file : files) {
                if (file instanceof PsiFile) {
                    nextStates.add(currentState.withAdjustedCurrentNode(new SearchNode.PsiFileNode((PsiFile) file)));
                }
            }
            return nextStates;
        }

        @Override
        public String errorString() {
            return "Cannot find referenced file";
        }
    }

    /** navigate to the files that are referencing the current nodes file */
    public static class GoToReferencingFile extends SearchStep {
        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            String uri = currentState.getCurrentNode().fileUri();
            Collection<PsiFile> files = AssetFileNavigator.findReferencingAssetFiles(uri);
            if (files.isEmpty()) {
                return Collections.emptySet();
            }
            Set<SearchState> nextStates = new HashSet<>();
            for (PsiFile file : files) {
                nextStates.add(currentState.withAdjustedCurrentNode(new SearchNode.PsiFileNode(file)));
            }
            return nextStates;
        }

        @Override
        public String errorString() {
            return "No one is referencing that file";
        }

        @Override
        public boolean needsFileGraph() {
            return true;
        }
    }

    /** navigate to the occurrences of a regular expression inside the current file, using previously collected variables */
    public static class FindRegexInFile extends SearchStep {
        private final String regex;

        public FindRegexInFile(String regex) {
            this.regex = regex;
        }

        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            SearchNode currentNode = currentState.getCurrentNode();
            if (!(currentNode instanceof SearchNode.PsiFileNode)) {
                return Collections.emptyList();
            }
            String replacedRegex = regex;
            for (String variableName : currentState.allVariableNames()) {
                String replacement = Pattern.quote(currentState.getVariable(variableName).content());
                replacedRegex = replacedRegex.replace("$" + variableName, replacement);
            }

            PsiFile file = (PsiFile) currentNode.asPsi();
            String content = AssetFileNavigator.fileContent(file);

            Set<SearchState> nextStates = new HashSet<>();
            Matcher m = Pattern.compile(replacedRegex).matcher(content);
            while (m.find()) {
                int start = m.start();
                PsiElement matchedElement = file.findElementAt(start);
                nextStates.add(currentState.withAdjustedCurrentNode(new SearchNode() {
                    @Override
                    public PsiElement asPsi() {
                        return matchedElement;
                    }

                    @Override
                    public String content() {
                        return "";
                    }

                    @Override
                    public String name() {
                        return "";
                    }
                }));
            }

            return nextStates;
        }

        @Override
        public String errorString() {
            return "Cannot find \"" + regex + "\" in file";
        }
    }

    /** apply a custom java function to a stored variable value */
    public static class ExecuteCustomFunction extends SearchStep {

        private final String functionName;
        private final String variableName;

        public ExecuteCustomFunction(String functionName, String variableName) {
            this.functionName = functionName;
            this.variableName = variableName;
        }

        @NotNull
        @Override
        public Collection<SearchState> apply(SearchState currentState, SyntaxBasedAssetNavigation context) {
            SearchNode variable = currentState.getVariable(variableName);
            if (variable == null) {
                return Collections.emptySet();
            }
            Function<String, String> function = context.functionNamed(functionName);
            if (function == null) {
                return Collections.emptySet();
            }
            String input = variable.content();
            try {
                String output = function.apply(input);
                return Collections.singleton(currentState.withUpdatedVariable(variableName, new SearchNode() {
                    @Override
                    public PsiElement asPsi() {
                        return variable.asPsi();
                    }

                    @Override
                    public String content() {
                        return output;
                    }

                    @Override
                    public String name() {
                        return variable.name();
                    }
                }));
            } catch (Exception e) {
                return Collections.emptyList();
            }
        }

        @Override
        public String errorString() {
            return "Failed to perform \"" + functionName + "\" on \"" + variableName + "\".";
        }
    }

}
