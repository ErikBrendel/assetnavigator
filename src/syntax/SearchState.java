package syntax;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * A "current progress" of a navigation, consisting of the current node and all variables so far
 */
public class SearchState {
    /** where we currently are */
    private SearchNode currentNode;

    /** all variables collected so far */
    private Map<String, SearchNode> variables = new HashMap<>();

    /** nobody except us creates us */
    private SearchState() {}

    /** create a new SearchState from a search node with no variables */
    public static SearchState fromInitialNode(SearchNode initialNode) {
        SearchState state = new SearchState();
        state.currentNode = initialNode;
        return state;
    }

    /** the current head of the navigation */
    public SearchNode getCurrentNode() {
        return currentNode;
    }

    /**
     * @return the SearchNode that was saved as variable under the given name, or null if no such variable exists
     */
    public SearchNode getVariable(String name) {
        return variables.getOrDefault(name, null);
    }

    /**
     * @return the complete list of all variable names
     */
    public Collection<String> allVariableNames() {
        return variables.keySet();
    }

    /**
     * create a new SearchState that is like this one, but with a different "current node"
     * @param newCurrentNode the current node the resulting SearchState should have
     */
    public SearchState withAdjustedCurrentNode(SearchNode newCurrentNode) {
        SearchState newState = new SearchState();
        newState.currentNode = newCurrentNode;
        // this might be not needed anymore when SearchSteps reference their parent
        // search steps in the future and can query the variables from there
        newState.variables.putAll(this.variables);
        return newState;
    }

    /**
     * create a new SearchState that is like this one, but with the current node
     * added to the set of variables under the given name
     */
    public SearchState withCurrentNodeAsVariable(String variableName) {
        return withUpdatedVariable(variableName, getCurrentNode());
    }

    /**
     * create a new SearchState that is like this one, but with the given variable
     * updated to have the new given value
     */
    public SearchState withUpdatedVariable(String variableName, SearchNode value) {
        SearchState newState = new SearchState();
        newState.variables.putAll(this.variables);
        newState.variables.put(variableName, value);
        newState.currentNode = this.currentNode;
        return newState;
    }

    /**
     * produce a debug message that shows all variables of this SearchState
     */
    public String dumpVariables() {
        StringBuilder result = new StringBuilder("[");

        for (Map.Entry<String, SearchNode> entry : variables.entrySet()) {
            result.append(entry.getKey()).append("=").append(entry.getValue().content()).append(",");
        }
        result.append("]\n");
        return result.toString();
    }
}
