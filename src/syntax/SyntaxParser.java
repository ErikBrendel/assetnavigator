package syntax;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Parser for the navigation DSL
 */
public class SyntaxParser {

    /**
     * the pattern that each xml tag must match,
     * in order to separate syntax errors from go-to-child-node commands
     */
    private static final Pattern TAG_NAME_PATTERN = Pattern.compile("[a-zA-Z]+");

    /**
     * Entrypoint for the parser: Create a navigation object from its source code
     */
    public static SyntaxBasedAssetNavigation parse(String syntaxString) {
        SearchStep[] steps = parseSteps(syntaxString.split("/"));
        return new SyntaxBasedAssetNavigation(Arrays.asList(steps));
    }

    /**
     * parse an array of step source code into their SearchStep - objects
     */
    private static SearchStep[] parseSteps(String[] stepStrings) {
        SearchStep[] steps = new SearchStep[stepStrings.length];
        for (int i = 0; i < stepStrings.length; i++) {
            steps[i] = parseStep(stepStrings[i]);
        }
        return steps;
    }

    /**
     * parse a single SearchStep
     */
    private static SearchStep parseStep(String stepString) {
        switch (stepString) { // translate the non-parameterizable steps
            case "":
                return new SearchStep.Void();
            case "..":
                return new SearchStep.GoToParent();
            case "*":
                return new SearchStep.FindAllChildren();
            case ">>":
                return new SearchStep.GoToReferencedFile();
            case "<<":
                return new SearchStep.GoToReferencingFile();
            case "!!info":
                return new SearchStep.ErrorLevelInfo();
            case "!!warn":
                return new SearchStep.ErrorLevelWarn();
            case "!!error":
                return new SearchStep.ErrorLevelError();
        }

        // translate the parameterized steps by splitting away the step identifier
        // and passing the parameter as argument to the step constructor
        if (stepString.startsWith("..")) {
            String parentName = stepString.substring("..".length());
            return new SearchStep.GoToNamedParent(parentName);
        } else if (stepString.startsWith("[]")) {
            String attributeName = stepString.substring("[]".length());
            return new SearchStep.GoToAttribute(attributeName);
        } else if (stepString.startsWith("=$")) {
            String saveName = stepString.substring("=$".length());
            return new SearchStep.SaveCurrentNode(saveName);
        } else if (stepString.startsWith(">$")) {
            String saveName = stepString.substring(">$".length());
            return new SearchStep.GoToSaveMark(saveName);
        } else if (stepString.startsWith("==$")) {
            String saveName = stepString.substring("==$".length());
            return new SearchStep.AssertContentEqualsOther(saveName);
        } else if (stepString.startsWith("==<>")) {
            String compareString = stepString.substring("==<>".length());
            return new SearchStep.AssertNameEqualsLiteral(compareString);
        } else if (stepString.startsWith("==''")) {
            String compareString = stepString.substring("==<>".length());
            return new SearchStep.AssertContentEqualsLiteral(compareString);
        } else if (stepString.startsWith("::")) {
            String regexString = stepString.substring("::".length());
            return new SearchStep.FindRegexInFile(regexString);
        } else if (stepString.startsWith("(") && stepString.contains(")$")) {
            String[] data = stepString.split("\\$");
            String functionName = data[0].substring(1, data[0].length() - 1);
            return new SearchStep.ExecuteCustomFunction(functionName, data[1]);
        }

        // if we reach this, it might just be the name of a child tag to visit
        if (TAG_NAME_PATTERN.matcher(stepString).matches()) {
            return new SearchStep.FindChild(stepString);
        }

        // we could not understand this step, so we add an error step to the navigation
        return new SearchStep.ErrorMessage("Cannot parse step: \"" + stepString + "\"");
    }
}
