package syntax;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.xml.XmlAttribute;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A SearchNode is the common interface for different types of places where a SearchState can currently be.
 */
public abstract class SearchNode {

    /** convert this SearchNode to a PsiElement that represents it best */
    public abstract PsiElement asPsi();

    /** retrieve the content of this node */
    public abstract String content();

    /** retrieve the name of this node */
    public abstract String name();

    /**
     * @return the asset uri that points to the file that this node belongs to
     */
    public String fileUri() {
        String path = filePath();
        if (!path.contains("/default/")) {
            return path;
        }
        return "default/" + path.split("/default/", 2)[1];
    }

    /**
     * @return the full path of the file that this node belongs to
     */
    @NotNull
    protected String filePath() {
        return asPsi().getContainingFile().getVirtualFile().getPath();
    }

    /**
     * @return the parent node of this one, null when no parent can be found
     */
    public SearchNode parent() {
        return null;
    }

    /**
     * @return all children of this node that have the given name
     */
    public List<SearchNode> getChildrenWithName(String childName) {
        return Collections.emptyList();
    }

    /**
     * @return all children of this node
     */
    public List<SearchNode> getAllChildren() {
        return Collections.emptyList();
    }

    /**
     * @return the attribute of this node with the given name, null if no such attribute exists
     */
    public SearchNode getAttribute(String attributeName) {
        return null;
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    /**
     * An xml attribute as SearchNode.
     * Its parent is the containing xml tag.
     * It has no attributes.
     */
    public static class XmlAttributeNode extends SearchNode {

        private final XmlAttribute xmlAttribute;

        public XmlAttributeNode(XmlAttribute xmlAttribute) {
            this.xmlAttribute = xmlAttribute;
        }

        @Override
        public PsiElement asPsi() {
            return xmlAttribute;
        }

        @Override
        public String content() {
            return xmlAttribute.getValue();
        }

        @Override
        public String name() {
            return xmlAttribute.getName();
        }

        @Override
        public SearchNode parent() {
            return new XmlTagNode(xmlAttribute.getParent());
        }
    }

    /**
     * An xml tag as SearchNode.
     * Its tag is its name, the whole text in the sub-tree is its content.
     * The top level node has no parent.
     */
    public static class XmlTagNode extends SearchNode {

        private final XmlTag xmlNode;

        public XmlTagNode(XmlTag xmlNode) {
            this.xmlNode = xmlNode;
        }

        @Override
        public PsiElement asPsi() {
            return xmlNode;
        }

        @Override
        public String content() {
            if (xmlNode.getSubTags().length > 0) {
                return xmlNode.getSubTags().length + "children";
            }
            return xmlNode.getValue().getText();
        }

        @Override
        public String name() {
            return xmlNode.getName();
        }

        @Override
        public SearchNode parent() {
            XmlTag parentTag = xmlNode.getParentTag();
            if (parentTag == null) {
                return null;
            }
            return new XmlTagNode(parentTag);
        }

        @Override
        public List<SearchNode> getChildrenWithName(String childName) {
            XmlTag[] subTags = xmlNode.findSubTags(childName);
            List<SearchNode> result = new ArrayList<>(subTags.length);
            for (XmlTag child : subTags) {
                result.add(new XmlTagNode(child));
            }
            return result;
        }

        @Override
        public List<SearchNode> getAllChildren() {
            XmlTag[] subTags = xmlNode.getSubTags();
            List<SearchNode> result = new ArrayList<>(subTags.length);
            for (XmlTag child : subTags) {
                result.add(new XmlTagNode(child));
            }
            return result;
        }

        @Override
        public SearchNode getAttribute(String attributeName) {
            XmlAttribute attribute = xmlNode.getAttribute(attributeName);
            if (attribute == null) {
                return null;
            }
            return new XmlAttributeNode(attribute);
        }
    }

    /**
     * An arbitrary file as SearchNode.
     * Its name and content are the file name.
     * It has the root tag as single child, if it is an xml file.
     */
    public static class PsiFileNode extends SearchNode {

        private final PsiFile file;

        public PsiFileNode(PsiFile file) {
            this.file = file;
        }

        @Override
        public PsiElement asPsi() {
            return file;
        }

        @Override
        public String content() {
            return file.getName();
        }

        @NotNull
        @Override
        public String filePath() {
            return file.getVirtualFile().getPath();
        }

        @Override
        public String name() {
            return file.getName();
        }

        @Override
        public SearchNode parent() {
            return null;
        }

        @Override
        public List<SearchNode> getChildrenWithName(String childName) {
            List<SearchNode> allChildren = getAllChildren();
            if (!allChildren.isEmpty() && allChildren.get(0).name().equals(childName)) {
                return allChildren;
            }
            return Collections.emptyList();
        }

        @Override
        public List<SearchNode> getAllChildren() {
            if (file instanceof XmlFile) {
                XmlFile xmlFile = (XmlFile) file;
                XmlTag rootTag = xmlFile.getRootTag();
                if (rootTag != null) {
                    return Collections.singletonList(new XmlTagNode(rootTag));
                }
            }
            return Collections.emptyList();
        }
    }
}
