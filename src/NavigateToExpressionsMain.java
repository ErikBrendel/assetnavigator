import com.intellij.codeInsight.daemon.LineMarkerInfo;
import com.intellij.codeInsight.daemon.LineMarkerProvider;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.openapi.util.IconLoader;
import com.intellij.psi.PsiElement;
import com.intellij.psi.xml.XmlTag;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import syntax.SyntaxBasedAssetNavigation;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * Main entry point for all navigations defined via the navigation DSL.
 * All these navigations get set up during plugin startup, and then get executed
 * by the two inner classes. They exist to separate the fast from the slow navigations,
 * although this only works a little bit :(
 *
 * @see syntax/asset_navigation_syntax.txt for more details on this language
 *
 * @see <a href="http://www.jetbrains.org/intellij/sdk/docs/tutorials/custom_language_support/line_marker_provider.html">custom language &gt; line marker provider</a>
 * @see <a href="https://github.com/JetBrains/intellij-sdk-docs/tree/master/code_samples/simple_language_plugin/src/com/simpleplugin">simple language plugin</a>
 */
public class NavigateToExpressionsMain {

    private static final Icon DEFAULT_ICON = IconLoader.getIcon("/icons/go_to_green.png");
    private static final Icon UI_ICON = IconLoader.getIcon("/icons/go_to_ui.png");
    private static final Icon SHADER_ICON = IconLoader.getIcon("/icons/go_to_fragment_shader.png");
    private static final Icon SHADER_PROGRAM_ICON = IconLoader.getIcon("/icons/go_to_shader_program.png");
    private static final Icon IMPL_ICON = IconLoader.getIcon("/icons/go_to_impl.png");
    private static final Icon BACK_ICON = IconLoader.getIcon("/icons/go_back.png");
    private static final Icon PRESETS_ICON = IconLoader.getIcon("/icons/go_to_preset.png");
    private static final Icon TEXTURE_ICON = IconLoader.getIcon("/icons/go_to_texture.png");
    private static final Icon INPUT_ICON = IconLoader.getIcon("/icons/input.png");
    private static final Icon INPUT_ICON_VIDEO = IconLoader.getIcon("/icons/input_video.png");
    private static final Icon OUTPUT_ICON = IconLoader.getIcon("/icons/output.png");
    private static final Icon OUTPUT_FLIP_ICON_VIDEO = IconLoader.getIcon("/icons/output_flip_video.png");
    private static final Icon WRITE_TO_TEXTURE_ICON = IconLoader.getIcon("/icons/write_to_texture.png");
    private static final Icon READ_FROM_TEXTURE_ICON = IconLoader.getIcon("/icons/read_from_texture.png");

    private static void addNavigations() {
        addNavigation(SyntaxBasedAssetNavigation.fromString( //parameter definition to impl
                "[]id/=$parameterId/../..parameters/..vcadefinition/!!warn/[]implset/>>/" +
                        "vcaimplementationset/implementations/target/implementation/[]uri/>>/" +
                        "vcaimplementation/targets/target/parameters/*/[]id/==$parameterId/..",
                "parameter pass input connections", IMPL_ICON
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //pass input uniform to fragment shader uniform declaration
                "==<>passinput/[]id/=$uniformName/../..passinputs/..pass/=$myPass/!!error/" +
                        "passattributes/passattribute/[]name/==''shaderprogram/../value/=$myShaderProgram/" +
                        ">$myPass/../../shaderprograms/shaderprogram/[]id/==$myShaderProgram/../" +
                        "*/[]file/>>/(cleanUniformName)$uniformName/::uniform\\s+.+\\s+$uniformName",
                "fragment shader uniform declaration", SHADER_ICON
        ).registerFunction("cleanUniformName", (String s) -> s.replaceAll("\\[0]", "")));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //pass input semantic to fragment shader uniform declaration
                "==<>passinputsemantic/[]id/=$uniformName/../..passinputs/..pass/=$myPass/!!error/" +
                        "passattributes/passattribute/[]name/==''shaderprogram/../value/=$myShaderProgram/" +
                        ">$myPass/../../shaderprograms/shaderprogram/[]id/==$myShaderProgram/../" +
                        "*/[]file/>>/(cleanUniformName)$uniformName/::uniform\\s+.+\\s+$uniformName",
                "fragment shader uniform declaration", SHADER_ICON
        ).registerFunction("cleanUniformName", (String s) -> s.replaceAll("\\[0]", "")));

        addNavigation(SyntaxBasedAssetNavigation.fromString( //preset parameter usage to parameter def
                "==<>parameter/[]ref/=$parameterName/../..vca/..preset/..presets/!!error/<</" +
                        "vcadefinition/parameters/*/[]id/==$parameterName/..",
                "parameter definition", BACK_ICON
        ));

        addParameterImplNavigation();
        addShaderProgramNavigation();
        addTextureNavigation();
        addUiNavigation();
        addEffectIONavigation();
        addVideoXmlNavigations();
    }

    private static void addParameterImplNavigation() {
        addNavigation(SyntaxBasedAssetNavigation.fromString( // impl parameter back to def
                "[]id/=$parameterId/../..parameters/..target/<</<</!!error/" +
                        "vcadefinition/parameters/*/[]id/==$parameterId/..",
                "parameter definition", BACK_ICON
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //setpassinput to pass input
                "==<>setpassinput/[]pass/=$passId/../[]passinput/=$passInputId/!!warn/" +
                        "../../..parameters/../passes/pass/[]id/==$passId/../!!error/passinputs/passinput/[]id/==$passInputId/..",
                "pass input", DEFAULT_ICON
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //expressionset to pass input
                "==<>expressionset/[]pass/=$passId/../[]passinput/=$passInputId/!!warn/" +
                        "../../..parameters/../passes/pass/[]id/==$passId/../!!error/passinputs/passinput/[]id/==$passInputId/..",
                "pass input", DEFAULT_ICON
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //setoutputresolution to pass input
                "==<>setoutputresolution/[]pass/=$passId/../!!warn/" +
                        "../..parameters/../passes/pass/[]id/==$passId/../",
                "pass", DEFAULT_ICON
        ));
    }

    private static void addShaderProgramNavigation() {
        addNavigation(SyntaxBasedAssetNavigation.fromString( //pass shader program to shader program declaration
                "==<>shaderprogram/[]id/=$shaderProgramId/../..shaderprograms/../passes/!!warn/" +
                        "pass/[]id/=$passId/../passattributes/passattribute/[]name/==''shaderprogram/../value/==$shaderProgramId",
                "program usage", SHADER_PROGRAM_ICON,
                "$passId"
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //pass shader program to shader program declaration
                "==<>value/=$shaderProgramName/..passattribute/[]name/==''shaderprogram/../..passattributes/" +
                        "..pass/..passes/!!warn/../shaderprograms/shaderprogram/[]id/==$shaderProgramName/..",
                "shader program", SHADER_PROGRAM_ICON
        ));
    }

    private static void addTextureNavigation() {
        addNavigation(SyntaxBasedAssetNavigation.fromString( //texture to read usages
                "[]id/=$textureId/../..textures/../passes/pass/[]id/=$passId/../passinputs/" +
                        "passinput/[]id/=$inputId/../value/==$textureId",
                "read access", READ_FROM_TEXTURE_ICON,
                "$passId > $inputId"
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //texture to write usages
                "[]id/=$textureId/../..textures/../passes/pass/[]id/=$passId/../passoutputs/" +
                        "passoutput/[]id/=$outputId/../value/==$textureId",
                "write access", WRITE_TO_TEXTURE_ICON,
                "$passId > $outputId"
        ));


        addNavigation(SyntaxBasedAssetNavigation.fromString( //pass input texture to texture declaration
                "==<>value/=$textureName/..passinput/[]type/==''sampler/../..passinputs/..pass/..passes/!!warn/../" +
                        "textures/*/[]id/==$textureName/..",
                "texture declaration", TEXTURE_ICON
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //pass output to texture declaration
                "==<>value/=$textureName/..passoutput/..passoutputs/..pass/..passes/!!warn/../" +
                        "textures/*/[]id/==$textureName/..",
                "texture declaration", TEXTURE_ICON
        ));
    }

    private static void addUiNavigation() {
        addNavigation(SyntaxBasedAssetNavigation.fromString( //parameter definition to parameter ui entry
                "[]id/=$parameterId/../..parameters/..vcadefinition/!!warn/[]ui/>>/" +
                        "vcaui/target/parameters/parameter/[]id/==$parameterId/..",
                "ui entry", UI_ICON
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //ui parameter to parameter def
                "==<>parameter/[]id/=$parameterId/../..parameters/..target/..vcaui/!!error/<</" +
                        "vcadefinition/parameters/*/[]id/==$parameterId/..",
                "parameter definition", BACK_ICON
        ));


        addNavigation(SyntaxBasedAssetNavigation.fromString( //enum parameter element to its ui entry
                "==<>element/[]id/=$elementId/../..enumeration/..enumparameter/[]id/=$parameterId/../" +
                        "/..parameters/..vcadefinition/[]ui/>>/" +
                        "vcaui/target/parameters/parameter/[]id/==$parameterId/!!warn/../element/[]id/==$elementId/..",
                "ui entry", UI_ICON
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //ui enum element to parameter def element
                "==<>element/[]id/=$elementId/../..parameter/[]id/=$parameterId/../" +
                        "/..parameters/..target/..vcaui/!!error/<</" +
                        "vcadefinition/parameters/enumparameter/[]id/==$parameterId/../enumeration/element/[]id/==$elementId/..",
                "parameter definition", BACK_ICON
        ));


        addNavigation(SyntaxBasedAssetNavigation.fromString( //preset to ui entry
                "==<>preset/[]id/=$presetId/../..presets/..target/..presettargets/!!warn/<</" +
                        "vcadefinition/[]ui/>>/" +
                        "vcaui/target/presets/preset/[]id/==$presetId/..",
                "ui entry", UI_ICON
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //ui preset to preset def
                "==<>preset/[]id/=$presetId/../..presets/..target/..vcaui/!!error/<</" +
                        "vcadefinition/[]presets/>>/" +
                        "presettargets/target/presets/preset/[]id/==$presetId/..",
                "preset definition", BACK_ICON
        ));


        addNavigation(SyntaxBasedAssetNavigation.fromString( //parameter definition to parameter ui entry
                "[]id/=$brushId/../..brushes/..vcadefinition/!!warn/[]ui/>>/" +
                        "vcaui/target/brushes/brush/[]id/==$brushId/..",
                "ui entry", UI_ICON
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //ui brush to preset def
                "==<>brush/[]id/=$brushId/../..brushes/..target/..vcaui/!!error/<</" +
                        "vcadefinition/brushes/*/[]id/==$brushId/..",
                "brush definition", BACK_ICON
        ));
    }

    private static void addEffectIONavigation() {
        //things that are the same for effect inputs and effect outputs
        for (String inOrOut : new String[]{"in", "out"}) {
            boolean in = inOrOut.equals("in");

            addNavigation(SyntaxBasedAssetNavigation.fromString( //effect inOutput slot image to texture declaration
                    "==<>" + inOrOut + "put/[]type/==''image/../=$textureName/" +
                            ".." + inOrOut + "puts/..io/..vcadefinition/!!warn/[]implset/>>/" +
                            "vcaimplementationset/implementations/target/implementation/[]uri/>>/" +
                            "vcaimplementation/targets/target/textures/*/[]id/==$textureName/..",
                    "texture declaration", TEXTURE_ICON
            ));
            addNavigation(SyntaxBasedAssetNavigation.fromString( //texture to effect inOutPut
                    "[]id/=$textureName/../..textures/..target/..targets/..vcaimplementation/<</<</" +
                            "vcadefinition/io/" + inOrOut + "puts/" + inOrOut + "put/==$textureName/[]type/==''image/..",
                    "vca" + inOrOut + "put declaration", in ? INPUT_ICON : OUTPUT_ICON
            ));
        }
    }

    private static void addVideoXmlNavigations() {
        addNavigation(SyntaxBasedAssetNavigation.fromString( //connection destination stage
                "==<>connection/[]toStage/=$targetStage/../..connections/!!error/../" +
                        "stages/stage/[]id/==$targetStage/..",
                "destination stage", INPUT_ICON_VIDEO
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //connection source stage
                "==<>connection/[]fromStage/=$targetStage/../..connections/!!error/../" +
                        "stages/stage/[]id/==$targetStage/..",
                "source stage", OUTPUT_FLIP_ICON_VIDEO
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //stage outputs
                "==<>stage/[]id/=$id/../..stages/..videopipeline/" +
                        "connections/connection/[]fromStage/==$id",
                "stage outputs", OUTPUT_FLIP_ICON_VIDEO
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //stage inputs
                "==<>stage/[]id/=$id/../..stages/..videopipeline/" +
                        "connections/connection/[]toStage/==$id",
                "stage inputs", INPUT_ICON_VIDEO
        ));
        addNavigation(SyntaxBasedAssetNavigation.fromString( //from effect parameter value override to its definition
                "==<>parameter/[]id/=$parameterId/../..parameters/parameter/[]id/==''effectUri/../[]value/>>/" +
                        "vcadefinition/parameters/*/[]id/==$parameterId/..",
                "effect parameter definition", IMPL_ICON
        ));
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    private static final List<SyntaxBasedAssetNavigation> SIMPLE_NAVIGATIONS;
    private static final List<SyntaxBasedAssetNavigation> COMPLEX_NAVIGATIONS;

    static {
        SIMPLE_NAVIGATIONS = new ArrayList<>();
        COMPLEX_NAVIGATIONS = new ArrayList<>();
        addNavigations();
    }

    /**
     * register a navigation for execution
     */
    private static void addNavigation(SyntaxBasedAssetNavigation navigation) {
        if (navigation.needsFileGraph()) {
            COMPLEX_NAVIGATIONS.add(navigation);
        } else {
            SIMPLE_NAVIGATIONS.add(navigation);
        }
    }

    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    /**
     * entry point for the IDE for all navigations which
     * do not need the file graph and thus can be executed pretty fast
     */
    public static class Simple extends RelatedItemLineMarkerProvider {

        @Override
        protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo> result) {
            if (element instanceof XmlTag) {
                XmlTag tag = (XmlTag) element;
                for (SyntaxBasedAssetNavigation navigation : SIMPLE_NAVIGATIONS) {
                    navigation.addNavigationGutterIcon(tag, result);
                }
            }
        }

    }

    /**
     * entry point for the IDE for all navigations which need
     * the file graph and thus need to wait for its setup before they can get executed
     */
    public static class Complex implements LineMarkerProvider {

        @Nullable
        @Override
        public LineMarkerInfo getLineMarkerInfo(@NotNull PsiElement psiElement) {
            return null;
        }

        @Override
        public void collectSlowLineMarkers(@NotNull List<PsiElement> list, @NotNull Collection<LineMarkerInfo> collection) {
            for (PsiElement e : list) {
                myCollectNavigationMarkers(e, collection);
            }
        }

        private void myCollectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo> result) {
            if (element instanceof XmlTag) {
                XmlTag tag = (XmlTag) element;
                for (SyntaxBasedAssetNavigation navigation : COMPLEX_NAVIGATIONS) {
                    navigation.addNavigationGutterIcon(tag, result);
                }
            }
        }
    }

}
