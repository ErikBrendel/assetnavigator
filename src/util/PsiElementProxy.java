package util;

import com.intellij.lang.ASTNode;
import com.intellij.lang.Language;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.intellij.psi.meta.PsiMetaData;
import com.intellij.psi.meta.PsiMetaOwner;
import com.intellij.psi.scope.PsiScopeProcessor;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.SearchScope;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * awesome wrapper class around any PsiElement that does nothing,
 * which allows for further functional proxies to be written easier.
 */
public class PsiElementProxy implements PsiNamedElement, PsiMetaOwner {

    /** the PsiElement that gets wrapped by this class */
    protected final PsiElement wrapped;

    public PsiElementProxy(@NotNull PsiElement wrapped) {
        this.wrapped = wrapped;
    }

    @NotNull
    @Override
    public Project getProject() throws PsiInvalidElementAccessException {
        return wrapped.getProject();
    }

    @NotNull
    @Override
    public Language getLanguage() {
        return wrapped.getLanguage();
    }

    @Override
    public PsiManager getManager() {
        return wrapped.getManager();
    }

    @NotNull
    @Override
    public PsiElement[] getChildren() {
        return wrapped.getChildren();
    }

    @Override
    public PsiElement getParent() {
        return wrapped.getParent();
    }

    @Override
    public PsiElement getFirstChild() {
        return wrapped.getFirstChild();
    }

    @Override
    public PsiElement getLastChild() {
        return wrapped.getLastChild();
    }

    @Override
    public PsiElement getNextSibling() {
        return wrapped.getNextSibling();
    }

    @Override
    public PsiElement getPrevSibling() {
        return wrapped.getPrevSibling();
    }

    @Override
    public PsiFile getContainingFile() throws PsiInvalidElementAccessException {
        return wrapped.getContainingFile();
    }

    @Override
    public TextRange getTextRange() {
        return wrapped.getTextRange();
    }

    @Override
    public int getStartOffsetInParent() {
        return wrapped.getStartOffsetInParent();
    }

    @Override
    public int getTextLength() {
        return wrapped.getTextLength();
    }

    @Nullable
    @Override
    public PsiElement findElementAt(int i) {
        return wrapped.findElementAt(i);
    }

    @Nullable
    @Override
    public PsiReference findReferenceAt(int i) {
        return wrapped.findReferenceAt(i);
    }

    @Override
    public int getTextOffset() {
        return wrapped.getTextOffset();
    }

    @Override
    public String getText() {
        return wrapped.getText();
    }

    @NotNull
    @Override
    public char[] textToCharArray() {
        return wrapped.textToCharArray();
    }

    @Override
    public PsiElement getNavigationElement() {
        return wrapped.getNavigationElement();
    }

    @Override
    public PsiElement getOriginalElement() {
        return wrapped.getOriginalElement();
    }

    @Override
    public boolean textMatches(@NotNull CharSequence charSequence) {
        return wrapped.textMatches(charSequence);
    }

    @Override
    public boolean textMatches(@NotNull PsiElement psiElement) {
        return wrapped.textMatches(psiElement);
    }

    @Override
    public boolean textContains(char c) {
        return wrapped.textContains(c);
    }

    @Override
    public void accept(@NotNull PsiElementVisitor psiElementVisitor) {
        wrapped.accept(psiElementVisitor);
    }

    @Override
    public void acceptChildren(@NotNull PsiElementVisitor psiElementVisitor) {
        wrapped.acceptChildren(psiElementVisitor);
    }

    @Override
    public PsiElement copy() {
        return wrapped.copy();
    }

    @Override
    public PsiElement add(@NotNull PsiElement psiElement) throws IncorrectOperationException {
        return wrapped.add(psiElement);
    }

    @Override
    public PsiElement addBefore(@NotNull PsiElement psiElement, @Nullable PsiElement psiElement1) throws IncorrectOperationException {
        return wrapped.addBefore(psiElement, psiElement1);
    }

    @Override
    public PsiElement addAfter(@NotNull PsiElement psiElement, @Nullable PsiElement psiElement1) throws IncorrectOperationException {
        return wrapped.addAfter(psiElement, psiElement1);
    }

    @Override
    public void checkAdd(@NotNull PsiElement psiElement) throws IncorrectOperationException {
        wrapped.checkAdd(psiElement);
    }

    @Override
    public PsiElement addRange(PsiElement psiElement, PsiElement psiElement1) throws IncorrectOperationException {
        return wrapped.addRange(psiElement, psiElement1);
    }

    @Override
    public PsiElement addRangeBefore(@NotNull PsiElement psiElement, @NotNull PsiElement psiElement1, PsiElement psiElement2) throws IncorrectOperationException {
        return wrapped.addRangeBefore(psiElement, psiElement1, psiElement2);
    }

    @Override
    public PsiElement addRangeAfter(PsiElement psiElement, PsiElement psiElement1, PsiElement psiElement2) throws IncorrectOperationException {
        return wrapped.addRangeAfter(psiElement, psiElement1, psiElement2);
    }

    @Override
    public void delete() throws IncorrectOperationException {
        wrapped.delete();
    }

    @Override
    public void checkDelete() throws IncorrectOperationException {
        wrapped.checkDelete();
    }

    @Override
    public void deleteChildRange(PsiElement psiElement, PsiElement psiElement1) throws IncorrectOperationException {
        wrapped.deleteChildRange(psiElement, psiElement1);
    }

    @Override
    public PsiElement replace(@NotNull PsiElement psiElement) throws IncorrectOperationException {
        return wrapped.replace(psiElement);
    }

    @Override
    public boolean isValid() {
        return wrapped.isValid();
    }

    @Override
    public boolean isWritable() {
        return wrapped.isWritable();
    }

    @Nullable
    @Override
    public PsiReference getReference() {
        return wrapped.getReference();
    }

    @NotNull
    @Override
    public PsiReference[] getReferences() {
        return wrapped.getReferences();
    }

    @Nullable
    @Override
    public <T> T getCopyableUserData(Key<T> key) {
        return wrapped.getCopyableUserData(key);
    }

    @Override
    public <T> void putCopyableUserData(Key<T> key, @Nullable T t) {
        wrapped.putCopyableUserData(key, t);
    }

    @Override
    public boolean processDeclarations(@NotNull PsiScopeProcessor psiScopeProcessor, @NotNull ResolveState resolveState, @Nullable PsiElement psiElement, @NotNull PsiElement psiElement1) {
        return wrapped.processDeclarations(psiScopeProcessor, resolveState, psiElement, psiElement1);
    }

    @Nullable
    @Override
    public PsiElement getContext() {
        return wrapped.getContext();
    }

    @Override
    public boolean isPhysical() {
        return wrapped.isPhysical();
    }

    @NotNull
    @Override
    public GlobalSearchScope getResolveScope() {
        return wrapped.getResolveScope();
    }

    @NotNull
    @Override
    public SearchScope getUseScope() {
        return wrapped.getUseScope();
    }

    @Override
    public ASTNode getNode() {
        return wrapped.getNode();
    }

    @Override
    public boolean isEquivalentTo(PsiElement psiElement) {
        return wrapped.isEquivalentTo(psiElement);
    }

    @Override
    public Icon getIcon(int i) {
        return wrapped.getIcon(i);
    }

    @Nullable
    @Override
    public <T> T getUserData(@NotNull Key<T> key) {
        return wrapped.getUserData(key);
    }

    @Override
    public <T> void putUserData(@NotNull Key<T> key, @Nullable T t) {
        wrapped.putUserData(key, t);
    }

    @Nullable
    @Override
    public String getName() {
        if (wrapped instanceof PsiNamedElement) {
            return ((PsiNamedElement) wrapped).getName();
        }
        return "";
    }

    @Override
    public PsiElement setName(@NotNull String s) throws IncorrectOperationException {
        if (wrapped instanceof PsiNamedElement) {
            return ((PsiNamedElement) wrapped).setName(s);
        }
        return this;
    }

    @Nullable
    @Override
    public PsiMetaData getMetaData() {
        if (wrapped instanceof PsiMetaOwner) {
            return ((PsiMetaOwner) wrapped).getMetaData();
        }
        return null;
    }
}
