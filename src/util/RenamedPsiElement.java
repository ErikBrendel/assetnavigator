package util;

import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A specific PsiElement proxy that gives the element a different name
 */
public class RenamedPsiElement extends PsiElementProxy {

    private final String newName;

    /**
     * @param e the PsiElement that should get renamed
     * @param newName the new name that it will have through this proxy
     */
    public RenamedPsiElement(@NotNull PsiElement e, @NotNull String newName) {
        super(e);
        this.newName = newName;
    }

    @Nullable
    @Override
    public String getName() {
        return newName;
    }
}
