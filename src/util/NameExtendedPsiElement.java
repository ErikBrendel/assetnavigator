package util;

import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A specific PsiElement proxy that appends a given string to the original name of the element
 */
public class NameExtendedPsiElement extends PsiElementProxy {

    /** the suffix to append */
    private final String nameExtension;

    /**
     * @param wrapped the PsiElement to rename
     * @param nameExtension the suffix to append to its name
     */
    public NameExtendedPsiElement(@NotNull PsiElement wrapped, @NotNull String nameExtension) {
        super(wrapped);
        this.nameExtension = nameExtension;
    }

    @Nullable
    @Override
    public String getName() {
        return super.getName() + nameExtension;
    }
}
