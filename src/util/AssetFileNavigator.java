package util;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import util.graph.ReferenceCache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * The main entry point for the navigation system to access the asset structure.
 * It bloats up localized URIs and forwards requests to the ReferenceCache
 */
public class AssetFileNavigator {

    public static String fileContent(PsiFile file) {
        return ReferenceCache.fileContent(file);
    }

    //LOCALIZATION

    public static final String LOCALE_PLACEHOLDER = "@locale";
    private static final String[] LOCALES = {"default", "de", "en"};
    public static final String LOCALE_PLACEHOLDER_EMBEDDED = localeEmbed(LOCALE_PLACEHOLDER);
    private static final String[] LOCALES_EMBEDDED;

    static {
        LOCALES_EMBEDDED = new String[LOCALES.length];
        for (int l = 0; l < LOCALES.length; l++) {
            LOCALES_EMBEDDED[l] = localeEmbed(LOCALES[l]);
        }
    }

    /**
     * convert a possibly locale-placeholder-containing asset uri
     * to the list of all concrete uris it represents
     */
    public static List<String> inflateUri(String uri) {
        if (!isLocaleUrl(uri)) {
            //nothing to inflate here
            return Collections.singletonList(uri);
        }
        if (uri.contains(".xml:")) {
            // just ignore localization string dictionaries for now (see issue #3)
            uri = uri.split("\\.xml:")[0] + ".xml";
        }
        // replace the locale placeholders by all possible locales
        List<String> inflatedUris = new ArrayList<>(LOCALES.length);
        for (String locale : LOCALES) {
            inflatedUris.add(uri.replace(LOCALE_PLACEHOLDER, locale));
        }
        return inflatedUris;
    }

    /**
     * embed a locale into two path separators
     */
    private static String localeEmbed(String locale) {
        return "/" + locale + "/";
    }

    /**
     * does this uri even contain a localization placeholder?
     */
    public static boolean isLocaleUrl(String uri) {
        return uri.contains(LOCALE_PLACEHOLDER_EMBEDDED);
    }

    /**
     * Does this uri could be a localized one? Example:
     *  default/test/de/foo.bar
     *  could be a localized uri, that derived from
     *  default/test/@locale/foo.bar
     */
    public static boolean couldBeLocalizedUri(String uri) {
        for (String locale : LOCALES_EMBEDDED) {
            if (uri.contains(locale)) {
                return true;
            }
        }
        return false;
    }

    /**
     * find all the files that match the given uri string
     * @param possibleUri maybe an uri to a single or multiple asset files
     * @return all files that match this uri
     */
    public static Collection<PsiElement> findReferencedAssetFiles(String possibleUri) {
        Collection<PsiElement> assetDefinitions;
        List<String> uris = inflateUri(possibleUri);
        if (uris.size() == 1) {
            assetDefinitions = ReferenceCache.getInstance().findReferencedAssetFilesInternal(uris.get(0));
        } else {
            assetDefinitions = new ArrayList<>();
            for (String uri : uris) {
                assetDefinitions.addAll(ReferenceCache.getInstance().findReferencedAssetFilesInternal(uri));
            }
        }
        return assetDefinitions;
    }

    /**
     * Find all files that are referencing the given uri.
     * Also finds references with locale placeholders in them
     */
    public static Collection<PsiFile> findReferencingAssetFiles(String uri) {
        if (!couldBeLocalizedUri(uri)) {
            return ReferenceCache.getInstance().findReferencingAssetFilesInternal(uri);
        }
        List<PsiFile> files = new ArrayList<>(ReferenceCache.getInstance().findReferencingAssetFilesInternal(uri));
        for (String locale : LOCALES_EMBEDDED) {
            String adjustedUri = uri.replaceAll(locale, LOCALE_PLACEHOLDER_EMBEDDED);
            if (!adjustedUri.equals(uri)) {
                files.addAll(ReferenceCache.getInstance().findReferencingAssetFilesInternal(adjustedUri));
            }
        }
        return files;
    }

}
