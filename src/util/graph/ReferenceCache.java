package util.graph;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.util.Pair;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.intellij.openapi.vfs.newvfs.BulkFileListener;
import com.intellij.openapi.vfs.newvfs.events.*;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiFileSystemItem;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.messages.MessageBusConnection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import util.Deferred;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * facade onto the VFS
 */
public class ReferenceCache {

    /** list of file extensions that might belong to the asset file structure and should appear in the file graph */
    private static final String[] ASSET_FILE_TYPE_STRINGS = {"svg", "vert", "frag", "glsl", "comp", "xml", "png", "jpg", "jpeg", "bmp"};

    private static ReferenceCache instance = null;
    public static ReferenceCache getInstance() {
        if (instance == null) {
            instance = new ReferenceCache();
        }
        return instance;
    }

    /** the lazily loading file graph */
    private Deferred<FileGraph> deferredGraph = null;
    /** the file system changes listener */
    private VFSListener listener = new VFSListener();
    /**
     * The base path to the asset directory, ending with a "/"
     * If it is null, no asset directory was found
     */
    private String assetsRootUri = null;

    public ReferenceCache() {
        invalidateCache();
        findAssetRoot();
        listener.connectMessageBus();
    }

    /**
     * search for the base path that points to the asset directory
     */
    private void findAssetRoot() {
        Project project = ProjectManager.getInstance().getOpenProjects()[0];
        GlobalSearchScope globalSearchScope = GlobalSearchScope.allScope(project);
        for (String type : ASSET_FILE_TYPE_STRINGS) {
            Collection<VirtualFile> allFilesByExt = FilenameIndex.getAllFilesByExt(project, type, globalSearchScope);
            for (VirtualFile virtualFile : allFilesByExt) {
                String[] split = virtualFile.getUrl().split("/default/", 2);
                if (split.length > 1) {
                    assetsRootUri = split[0] + "/";
                    System.out.println("Found asset root: " + assetsRootUri);
                    return;
                }
            }
        }
        assetsRootUri = null;
        System.err.println("No asset root found!");
    }

    /**
     * replace the current file graph by a not yet loaded one
     */
    public void invalidateCache() {
        deferredGraph = new Deferred<>(this::loadGraph);
    }

    /**
     * Create a FileGraph that represents the current asset structure
     */
    private FileGraph loadGraph() {
        FileGraph graph = new FileGraph();

        Project project = ProjectManager.getInstance().getOpenProjects()[0];
        GlobalSearchScope globalSearchScope = GlobalSearchScope.allScope(project);

        //find all uris that are even valid
        Set<Pair<String, VirtualFile>> uris = new HashSet<>();
        for (String type : ASSET_FILE_TYPE_STRINGS) {
            Collection<VirtualFile> allFilesByExt = FilenameIndex.getAllFilesByExt(project, type, globalSearchScope);

            for (VirtualFile virtualFile : allFilesByExt) {
                String[] split = virtualFile.getUrl().split("/default/", 2);
                if (split.length > 1) {
                    uris.add(new Pair<>("default/" + split[1], virtualFile));
                }
            }
        }

        //scan all xml files and check which uris they contain (which asset files they reference)
        Collection<VirtualFile> xmlFiles = FilenameIndex.getAllFilesByExt(project, "xml", globalSearchScope);
        for (VirtualFile xmlFile : xmlFiles) {
            if (!isAssetFile(xmlFile)) {
                continue;
            }
            PsiFile assetFile = PsiManager.getInstance(project).findFile(xmlFile);
            if (assetFile == null) {
                continue;
            }
            String content = fileContent(assetFile);
            for (Pair<String, VirtualFile> uri : uris) {
                if (content.contains(uri.getFirst())) {
                    graph.connect(xmlFile, uri.getSecond());
                }
            }
        }
        System.out.println(graph);
        return graph;
    }

    /**
     * is this even a file that belongs to the asset directory?
     */
    private boolean isAssetFile(VirtualFile file) {
        return file.getPath().contains("/default/");
    }

    /**
     * without the file graph help, find all the files that match the given uri
     */
    public Collection<PsiElement> findReferencedAssetFilesInternal(String uri) {
        VirtualFile fileByUri = getFileByUri(uri);
        if (fileByUri == null) {
            return Collections.emptySet();
        }
        if (fileByUri.isDirectory()) {
            // return the list of entries in this directory
            Set<PsiElement> childFiles = new HashSet<>();
            for (VirtualFile child : fileByUri.getChildren()) {
                PsiElement psiFile = virtualToPsi(child);
                if (psiFile != null) {
                    childFiles.add(psiFile);
                }
            }
            return childFiles;
        }
        // return that file alone
        return Collections.singleton(virtualToPsi(fileByUri));
    }

    /**
     * convert a given VirtualFile into the corresponding Psi Element
     */
    @Nullable
    private PsiFileSystemItem virtualToPsi(@NotNull VirtualFile fileByUrl) {
        PsiManager psiManager = PsiManager.getInstance(ProjectManager.getInstance().getOpenProjects()[0]);
        if (fileByUrl.isDirectory()) {
            return psiManager.findDirectory(fileByUrl);
        } else {
            return psiManager.findFile(fileByUrl);
        }
    }

    /**
     * ask the file graph for all the files that reference the given one
     */
    public Collection<PsiFile> findReferencingAssetFilesInternal(String uri) {
        VirtualFile fileByUri = getFileByUri(uri);
        if (fileByUri == null) {
            return Collections.emptySet();
        }
        return deferredGraph.get().getReferencingFiles(fileByUri);
    }

    /**
     * @return the VirtualFile that belongs to an asset uri
     */
    @Nullable
    private VirtualFile getFileByUri(String uri) {
        if (assetsRootUri == null) {
            return null;
        }
        return VirtualFileManager.getInstance().findFileByUrl(assetsRootUri + uri);
    }

    /**
     * @return the full content of the given file as string
     */
    public static String fileContent(PsiFile file) {
        Document document = FileDocumentManager.getInstance().getDocument(file.getVirtualFile());
        if (document == null) {
            return "";
        }
        return document.getText();
    }

    /**
     * This class is capable of receiving changes in the file system (also file content changes)
     * It updates the FileGraph accordingly.
     */
    private class VFSListener implements BulkFileListener {

        private MessageBusConnection vfsConnection;

        public void connectMessageBus() {
            vfsConnection = ApplicationManager.getApplication().getMessageBus().connect();
            vfsConnection.subscribe(VirtualFileManager.VFS_CHANGES, this);
        }

        @Override
        public void after(@NotNull List<? extends VFileEvent> events) {
            //we are only interested in events that already happened, so we use the #after - method
            for (VFileEvent event : events) {
                if (eventIsImportant(event)) {

                    if (event instanceof VFileDeleteEvent) {
                        cachePathRemove(event.getPath());
                    } else if (event instanceof VFileContentChangeEvent) {
                        cacheContentChange(event.getFile());
                    } else if(event instanceof VFileCopyEvent) {
                        VFileCopyEvent copyEvent = (VFileCopyEvent) event;
                        VirtualFile newFile = copyEvent.getNewParent().findChild(copyEvent.getNewChildName());
                        cacheContentChange(newFile);
                    } else if(event instanceof VFileCreateEvent) {
                        cacheContentChange(event.getFile());
                    } else if(event instanceof VFileMoveEvent) {
                        VFileMoveEvent moveEvent = (VFileMoveEvent) event;
                        cachePathRemove(moveEvent.getOldPath());
                        cacheContentChange(moveEvent.getFile());
                    } else if (event instanceof VFilePropertyChangeEvent) {
                        //ignore it
                    } else {
                        System.err.println("Unknown event type: " + event);
                    }
                }
            }
        }

        /**
         * we only care about changes in the asset structure
         * @return whether the file graph needs to get adjusted because of this event
         */
        private boolean eventIsImportant(VFileEvent event) {
            return event.getPath().contains("/default/");
        }

        /**
         * the pattern to extract asset reference uris out of xml files
         */
        final Pattern uriPattern = Pattern.compile("[\">](default/[^<\"]+)[\"<]", Pattern.MULTILINE);

        /**
         * adjust the file graph by re-calculating which references this file has
         * @param file the file which might now reference other files than before
         */
        private void cacheContentChange(VirtualFile file) {
            PsiFile assetFile = PsiManager.getInstance(ProjectManager.getInstance().getOpenProjects()[0]).findFile(file);
            if (assetFile == null) {
                return;
            }
            String fileContent = fileContent(assetFile);
            final Matcher matcher = uriPattern.matcher(fileContent);

            List<VirtualFile> uris = new ArrayList<>();
            while (matcher.find()) {
                System.out.println("Full match: " + matcher.group(0));
                String uri = matcher.group(1);
                VirtualFile referencedFile = getFileByUri(uri);
                if (referencedFile != null) {
                    uris.add(referencedFile);
                }
            }
            if (deferredGraph.loadingStartedOrFinished()) {
                deferredGraph.get().updateFile(file, uris);
            }
        }

        /**
         * adjust the file graph by removing the specified file
         * @param path the file that no longer exists
         */
        private void cachePathRemove(String path) {
            if (deferredGraph.loadingStartedOrFinished()) {
                deferredGraph.get().removeFile(getFileByUri(path));
            }
        }
    }
}
