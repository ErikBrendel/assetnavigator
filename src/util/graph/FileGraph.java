package util.graph;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * A graph structure that represents VirtualFile - objects and connections between them.
 * The connections are stored in a doubly-linked fashion to allow for
 * fast forward and backward navigation as well as fast structural changes.
 */
public class FileGraph {

    /** the node of the graph for each VirtualFile */
    private Map<VirtualFile, FileNode> nodes = new HashMap<>();

    /**
     * get the list of all files that are referenced by the given one
     * (forward through the graph)
     */
    Collection<PsiFile> getReferencedFiles(VirtualFile sourceFile) {
        FileNode fileNode = nodes.get(sourceFile);
        if (fileNode == null) {
            System.err.println("no such file in cache: " + sourceFile.getPath());
            return Collections.emptySet();
        }
        return getVirtualFiles(fileNode.getOutputs());
    }

    /**
     * get the list of all files that are referencing the given one
     * (backwards through the graph)
     */
    Collection<PsiFile> getReferencingFiles(VirtualFile destination) {
        FileNode fileNode = nodes.get(destination);
        if (fileNode == null) {
            System.err.println("no such file in cache: " + destination.getPath());
            return Collections.emptySet();
        }
        return getVirtualFiles(fileNode.getInputs());
    }

    /**
     * add a new edge to the file graph
     */
    public void connect(VirtualFile from, VirtualFile to) {
        FileNode fromNode = getNode(from);
        FileNode toNode = getNode(to);
        fromNode.connectTo(toNode);
    }

    /**
     * converts a list of FileNode - objects to their corresponding PsiFiles
     */
    @NotNull
    private Collection<PsiFile> getVirtualFiles(Collection<FileNode> nodes) {
        Project project = ProjectManager.getInstance().getOpenProjects()[0];
        PsiManager psiManager = PsiManager.getInstance(project);

        List<PsiFile> result = new ArrayList<>(nodes.size());
        for (FileNode node : nodes) {
            result.add(psiManager.findFile(node.getFile()));
        }
        return result;
    }

    /**
     * remove a node from the graph, carefully removing all edges from and to it as well
     */
    public void removeFile(VirtualFile file) {
        if (!nodes.containsKey(file)) {
            return;
        }
        FileNode fileNode = nodes.get(file);
        fileNode.disconnectAll();
        nodes.put(file, null);
    }

    /**
     * replace the set of outgoing connections of a node by a new one, removing all the old edges
     * @param file the file which is now referencing a different set of files than before
     * @param newConnections which other files it references
     */
    public void updateFile(VirtualFile file, Collection<VirtualFile> newConnections) {
        Collection<FileNode> newOutputs = new HashSet<>();
        for (VirtualFile connectedFile : newConnections) {
            newOutputs.add(getNode(connectedFile));
        }
        getNode(file).updateOutputs(newOutputs);
    }

    /**
     * accessor for the graph node of a VirtualFile, creating it if needed
     */
    @NotNull
    private FileNode getNode(VirtualFile connectedFile) {
        return nodes.computeIfAbsent(connectedFile, FileNode::new);
    }

    @Override
    public String toString() {
        return super.toString() + " with " + nodes.size() + " nodes";
    }

    /**
     * A single node in the file graph.
     * It stores its ingoing and outgoing connections,
     * and provides methods for manipulating them.
     */
    private class FileNode {
        /** the file that this node represents */
        private final VirtualFile file;
        /** all nodes that reference this one (ingoing connections) */
        private Collection<FileNode> inputs = new HashSet<>();
        /** all nodes that are referenced by this one (outgoing connections) */
        private Collection<FileNode> outputs = new HashSet<>();

        FileNode(VirtualFile file) {
            this.file = file;
        }

        public Collection<FileNode> getInputs() {
            return inputs;
        }

        public Collection<FileNode> getOutputs() {
            return outputs;
        }

        public VirtualFile getFile() {
            return file;
        }

        /**
         * add a new edge to the graph, from this node to the specified destination node
         */
        public void connectTo(FileNode destination) {
            outputs.add(destination);
            destination.inputs.add(this);
        }

        /**
         * completely replace all outgoing connections by a new set of edges
         * @param newOutputs the new collection of nodes that should be referenced by this one
         */
        public void updateOutputs(Collection<FileNode> newOutputs) {
            // first, clear all outgoing connections
            for (FileNode destination : outputs) {
                destination.inputs.remove(this);
            }
            outputs.clear();

            // then, create the new ones
            for (FileNode newOutput : newOutputs) {
                connectTo(newOutput);
            }
        }

        /**
         * removes all ingoing and outgoing connections of this node
         */
        public void disconnectAll() {
            for (FileNode destination : outputs) {
                destination.inputs.remove(this);
            }
            outputs.clear();
            for (FileNode source : inputs) {
                source.outputs.remove(this);
            }
            inputs.clear();
        }
    }

}
