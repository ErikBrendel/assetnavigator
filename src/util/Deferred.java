package util;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

/**
 * A thread safe lazy initialization of an object
 * @param <T> which type of object to create lazily
 * @see https://www.nosid.org/java8-threadsafe-lazy-initialization.html
 */
public final class Deferred<T> {

    /** the creation function. It is set to null to mark that the creation already happened */
    private volatile Supplier<T> supplier;
    /** the created object (which might be null) */
    private T object = null;
    /** the lock to guarantee that only one thread invokes the creation function */
    private AtomicBoolean startedLoading = new AtomicBoolean(false);
    /** a lock for executing the creation function */
    private final Object loadingLock = new Object();

    /**
     * @param supplier a function that generates and object of type T
     */
    public Deferred(Supplier<T> supplier) {
        this.supplier = Objects.requireNonNull(supplier);
    }

    /**
     * IntelliJ - specific way of running an asynchronous task
     */
    private void runParallel(Runnable runnable) {
        ApplicationManager.getApplication().executeOnPooledThread(() ->
                ApplicationManager.getApplication().runReadAction(runnable));
    }

    /**
     * blocking accessor to the created object
     * @return the result of the given supplier function
     */
    public T get() {
        if (startedLoading.compareAndSet(false, true)) {
            runParallel(() -> {
                synchronized (loadingLock) {
                    object = supplier.get();
                    supplier = null;
                    loadingLock.notifyAll();
                }
            });
        }
        synchronized (loadingLock) {
            while (supplier != null) {
                try {
                    loadingLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return object;
    }

    /**
     * @return whether this Deferred object at least already started loading its data
     */
    public boolean loadingStartedOrFinished() {
        return startedLoading.get();
    }
}